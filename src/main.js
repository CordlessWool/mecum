
// Import fortawesome
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { config, library } from '@fortawesome/fontawesome-svg-core'
import { faGithub, faGitlab, faStackOverflow, faLinkedinIn, faXing} from '@fortawesome/free-brands-svg-icons'
import { faExternalLinkAlt, faBook } from '@fortawesome/free-solid-svg-icons'
import { faPaperPlane } from '@fortawesome/free-regular-svg-icons';
import '@fortawesome/fontawesome-svg-core/styles.css'

config.autoAddCss = false;
library.add(faGithub, faGitlab, faLinkedinIn, faStackOverflow, faXing, faExternalLinkAlt, faPaperPlane, faBook);

// Import main css
import '~/assets/style/index.scss'

// Import default layout so we don't need to import it to every page
import DefaultLayout from '~/layouts/Default.vue'

// The Client API can be used here. Learn more: gridsome.org/docs/client-api
export default function (Vue, { router, head, isClient }) {
  
  // Set fontawesome as global component
  Vue.component('font-awesome', FontAwesomeIcon)
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
}