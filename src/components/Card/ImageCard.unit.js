import { shallowMount, config, createLocalVue } from '@vue/test-utils';
import ImageCard from './ImageCard.vue';
import sinon from 'sinon';

const imageComp = {
    name: 'g-image',
    template: '<img :alt="alt" :src="src" />',
    props: ['src', 'alt']
}

const iconComp = {
    name: 'font-awesome',
    template: '<svg />'
}

describe('ImageCard', () => {
    it('implement', () => {
        const wrapper = shallowMount(ImageCard, {
            propsData: {
                id: 'someId',
                headline: 'Wolfgang Rathgeb'
            }
        })

        expect(wrapper.text()).toBe('Wolfgang Rathgeb')
    })

    it('description', () => {
        const id = '8982312'; 
        const headline = 'headline';
        const description = '<p>I am a nice text to describe a project</p>';
        const image = '~/assets/images/test.jpg';
        const wrapper = shallowMount(ImageCard, {
            propsData: {
                id,
                headline,
                description,
                image
            },
            stubs: {
                'g-image': imageComp
            }
        })

        expect(wrapper.classes()).not.toContain('selected');

        const imageElement = wrapper.find('.project__image');
        const headlineElement = wrapper.find('.project__headline');
        const descriptionElement = wrapper.find('.project__description');
        
        expect(wrapper.find('.project__link').exists()).toBe(false);

        expect(imageElement.isVisible()).toBe(true);
        expect(imageElement.attributes('src')).toBe(image);
        expect(imageElement.attributes('alt')).toBe(headline);

        expect(headlineElement.isVisible()).toBe(true);
        expect(headlineElement.text()).toBe(headline);

        // do currently not work
        //expect(descriptionElement.isVisible()).toBe(false);
        expect(descriptionElement.find('p').html()).toBe(description);

        expect(wrapper).toMatchSnapshot();

        wrapper.setProps({
            selected: id
        })

        expect(descriptionElement.isVisible()).toBe(true);

        expect(wrapper).toMatchSnapshot();
    })

    it('select on click', async () => {
        const id = '8982312'; 
        const headline = 'headline';
        const description = '<p>I am a nice text to describe a project</p>';
        const image = '~/assets/images/test.jpg';
        const wrapper = shallowMount(ImageCard, {
            propsData: {
                id,
                headline,
                description,
                image
            },
            stubs: {
                'g-image': imageComp
            }
        })

        expect(wrapper.vm.selected).not.toBe(id);

        wrapper.find('.project').trigger('click');

        expect(wrapper.vm.$data.isSelected).toBe(true);

    })

    it('select on click', async () => {
        const id = '8982312'; 
        const headline = 'headline';
        const description = '<p>I am a nice text to describe a project</p>';
        const wrapper = shallowMount(ImageCard, {
            propsData: {
                id,
                headline,
                description
            }
        })

        wrapper.find('.project').trigger('click');

        expect(wrapper.emitted('select')[0]).toEqual([id]);
    })

    it('do not trigger emit on link click', async () => {
        const id = '8982312'; 
        const headline = 'headline';
        const description = '<p>I am a nice text to describe a project</p>';
        const image = '~/assets/images/test.jpg';
        const link = "https://dropanote.de";
        const wrapper = shallowMount(ImageCard, {
            propsData: {
                id,
                headline,
                description,
                image,
                link
            },
            stubs: {
                'g-image': imageComp,
                'font-awesome': iconComp
            },
        })



        const linkElement = wrapper.find('.project__link');

        expect(linkElement.isVisible()).toBeTruthy();
        expect(linkElement.attributes('href')).toBe(link);

        linkElement.trigger('click');

        expect(wrapper.vm.$data.isSelected).toBe(false);

        expect(wrapper).toMatchSnapshot();

    })
})