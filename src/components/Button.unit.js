import { shallowMount } from '@vue/test-utils'
import Button from './Button.vue'

const createButton = (props, slots) => {
    return shallowMount(Button, {
        propsData: {
            ...props
        },
        slots: {
            ...slots
        }
    })
}

describe('Button', () => {
    it('render button', () => {
        const text = "Drop a note";
        const wrapper = createButton({},{
            default: text
        });

        expect(wrapper.text()).toMatch(text);
        expect(wrapper.attributes('class')).toContain('button');
        expect(wrapper).toMatchSnapshot();

    })
    it('check defaults', () => {
        const wrapper = createButton();
        
        expect(wrapper.attributes('class')).not.toContain('animate');
        expect(wrapper.attributes('class')).not.toContain('circle');
        expect(wrapper.attributes('class')).not.toContain('blink');
    })

    it('with animation', () => {
        const wrapper = createButton({
            animate: true
        });
        
        expect(wrapper.attributes('class')).toContain('animate');
    })

    it('display as circle', () => {
        const wrapper = createButton({
            iconButton: true
        },{
            "icon-post": "<svg />"
        });
        
        expect(wrapper.attributes('class')).toContain('circle');
        expect(wrapper).toMatchSnapshot();
    })



    it('add blink', () => {
        const wrapper = createButton({
            blink: true
        });
        
        expect(wrapper.attributes('class')).toContain('blink');
    })

    
    
})