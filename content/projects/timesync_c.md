---
name: Locating in a Museum
date: 2018-03-18
image: "https://images.unsplash.com/photo-1518998053901-5348d3961a04?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=600&q=75"
link: "https://github.com/CordlessWool/time_sync_master_slave"
---

While study time at Beuth University, we developed a project to find orientation in a Museum. The main goal was to get the position of a Person and give information about the locations. We thought about Wlan or Bluetooth beacons but decide to use a sound signal.

For getting the position a device have to measure the difference of three signals. To get the time gap it is needed to have the three speakers synchronized, so one important part was time sync between the speaker controllers. The first Implementation was done in python because of the jitter we moved to C.

Languages and tools: C, Python, Raspberry PI