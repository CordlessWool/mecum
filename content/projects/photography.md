---
name: Photography
date: 2020-06-10
image: "https://images.unsplash.com/photo-1601496814526-ddb282688807?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=600&q=75"
link: "https://unsplash.com/@cordlesswool"
---

For a long time, I didn't like to take photos and prefered to write, but in summer 2020 I discovered a new passion. It's like writing stories a mood or situation thing. 

If the moment is impressive, I will not take a picture, because it disrupts the moment. So there will be no pictures from my most beautiful moments, but I try to transfer the beauty of simple things.
