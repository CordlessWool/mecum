---
name: "Loom 2.0"
date: 2019-02-02
image: "./loom_2.png"
link: "https://gitlab.com/CordlessWool/loom"
---

After years of supporting three GhostJS instances for my three blog topics ( story, travel, recipes ) I decide to combine them in one GhostJS instance and use one interface. Because of restrictions in GhostJS themes, I decide to connect via GhostJS API and build the frontend in Gridsome.

Languages and tools: Gridsome, Vue, JavaScript, CSS3, HTML5