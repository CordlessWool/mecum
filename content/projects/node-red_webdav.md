---
name: Node-RED Webdav
date: 2018-09-18
image: "https://images.unsplash.com/photo-1504406438164-c0e042535100?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NTB8fG5vZGUlMjByZWR8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=600&q=60"
link: "https://github.com/CordlessWool/node-red-node-webdav"
---

2018 I started to make my life easier and energy-efficient with home automation, but I don't want to trust companies. I connected different devices with Node-RED to get the information if I am at home, turn on or off a Sonos speaker, and my heater up or down.

To go one step further, I started to get information from documents via OCR to rename and sort them in folders on my Nextcloud instance. Because of the low quality of the OCR, I decide to wait until it will be improved. As a result, there currently only stays a Webdave plugin for Node-RED

Tools and languages: Node-RED, JavaScript