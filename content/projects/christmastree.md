---
name: Christmas tree
date: 2018-11-11
image: "./christmastree.jpeg"
link: "https://gitlab.com/CordlessWool/christmas-tree"
---

Started with a low-level university task to program a VGA transmission on an FPGA, we build up a Christmas tree in embedded C code. The size of the Tree and the amount of snow can be modified by switches.

Languages and Tools: VHDL, Embedded C, Xilinx FPGA