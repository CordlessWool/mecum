---
name: "Floting Point Unit"
date: 2019-02-02
image: "https://images.unsplash.com/photo-1597862624292-45748390b00e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=600&q=75"
link: "https://gitlab.com/CordlessWool/fpu_vhdl_serial"
---

Floating Point Units (FPU) are part of any PC and most CPUs. We don't have to think about whatever a Computer does to calculate decimal numbers. For a study project, we implemented an FPU in a Xilinx FPGA and used it with embedded C code.

Languages and tools: VHDL, Embedded C, Xilinx FPGA