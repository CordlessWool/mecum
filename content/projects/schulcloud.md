---
name: "HPI-Schul-Cloud"
date: 2019-04-01
image: "https://images.unsplash.com/photo-1578593139939-cccb1e98698c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&h=600&q=75"
link: "https://hpi-schul-cloud.de"
---

In 2020 Schools in Germany are mostly analog, also if corona does not allow to meet lots of people. The digitalization of schools got more necessary than before. So the project leaves its pilot status and goes production.

The HPI-Schul-Cloud should help schools to do this step and work together on distance. To solve this and do not reinvent the wheel, the project combines with other tools and finds a way to include them well. The most significant base is the management of consents and fulfilling the rules of German law.

Practices roles: Software engineer, Architect, Scrum Master, Bussiness Analyst

Used Languages and Tools: Vue, React, Feathersjs, Jira, Confluence, Docker, Docker-compose, Github, Travis, and some more