# Mecum

Mesum is a profile page theme based on [Gridsome](https://gridsome.org/) and is developed for my profile page: [Drop a note](https://dropanote.de).

## License

The Code is free to use and stays under the MIT license.
The content and images are private as long they are not from Unsplash.

## Change content

Most content is stored in the `content` folder. 
- **content/text/intro.md**: Intro (About me) text and meta description for seach engineens.
- **content/projects**: Includes all project, each file represent one project.
- **content/social.yml**: List of icons to other profiles.
- **content/text/legal-notice.md**: Text for legal notice to keep law.
- **gridsome.config.js**: siteName (author name) and siteDescription (title)
